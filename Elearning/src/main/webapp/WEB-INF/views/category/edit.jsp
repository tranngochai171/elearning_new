<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<div class="main-body">
	<div class="page-body">
		<div class="row">
			<div class="col-md-12 mb-2">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item"><a href="/admin"> <i
								class="feather icon-home"></i> Trang chủ
						</a></li>
						<li class="breadcrumb-item"><a
							href='<c:url value="<%=UrlConstants.URL_CATEGORY%>" />'>Danh
								mục</a></li>
						<li class="breadcrumb-item"><a
							href='<c:url value="<%=UrlConstants.URL_CATEGORY + UrlConstants.URL_EDIT%>" />'>Sửa
								danh mục</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						<h2 class="text-uppercase text-center">Sửa danh mục</h2>
						<p class="text-center text-danger">${message }</p>
					</div>
					<div class="card-block">
						<c:url
							value="<%=UrlConstants.URL_CATEGORY + UrlConstants.URL_EDIT%>"
							var="action" />
						<form:form action="${ action }" method="POST"
							modelAttribute="categoryDto">
							<div class="row">
								<div class="col-md-6 m-auto">
									<form:hidden path="id" />
									<div class="form-group">
										<label>Tiêu đề</label>
										<form:input path="title" cssClass="form-control" />
										<form:errors path="title" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Icon (Optional)</label>
										<form:hidden path="icon" />
										<div id="dZUpload" class="dropzone"
											data-upload='<c:url value="<%=UrlConstants.URL_FILE + UrlConstants.URL_UPLOAD%>"/>'
											data-load='<c:url value="<%=UrlConstants.URL_FILE + UrlConstants.URL_LOAD%>"/>'>
											<div class="dz-default dz-message"></div>
										</div>
									</div>
									<div class="form-group mt-3">
										<button type="submit" class="btn btn-primary m-b-0">Lưu
											lại</button>
										<a href='<c:url value="<%=UrlConstants.URL_CATEGORY%>" />'
											class="btn btn-secondary text-white">Quay lại</a>
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>