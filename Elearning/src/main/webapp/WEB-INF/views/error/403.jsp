<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<h1>Oops!</h1>
<h2>403 Not Allowed</h2>
<div class="error-details">Sorry, you are not allowed to enter here!</div>