<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en">

<head>
<title><tiles:insertAttribute name="title" /></title>
<!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- Meta -->
<meta charset="utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- Style start -->
<tiles:insertAttribute name="style" />
<!-- Style end -->
</head>

<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">

			<!-- Navbar start -->
			<tiles:insertAttribute name="navbar" />
			<!-- Navbar end -->
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<!-- Sidebar start -->
					<tiles:insertAttribute name="sidebar" />
					<!-- Sidebar end -->
					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<!-- Main-body start -->
							<tiles:insertAttribute name="body" />
							<!-- Main-body end -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Script start -->
	<tiles:insertAttribute name="script" />
	<!-- Script end -->
</body>

</html>