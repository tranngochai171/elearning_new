<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="main-body">
	<div class="page-body">
		<div class="row">
			<div class="col-md-12 mb-2">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item"><a href="/admin"> <i
								class="feather icon-home"></i> Trang chủ
						</a></li>
						<li class="breadcrumb-item"><a
							href='<c:url value="<%=UrlConstants.URL_VIDEO%>"/>'>Danh sách
								video</a></li>
						<li class="breadcrumb-item"><a
							href='<c:url value="<%=UrlConstants.URL_VIDEO + UrlConstants.URL_ADD%>"/>'>Thêm
								mới video</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						<h2 class="text-uppercase text-center">Thêm mới video</h2>
					</div>
					<div class="card-block">
						<form action="#" method="post">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Tiêu đề</label> <input type="text" name="title"
											class="form-control" />
									</div>
									<div class="form-group">
										<label>Url</label> <input type="text" name="url"
											class="form-control" />
									</div>
									<div class="form-group">
										<label>Hình đại diện (Optional)</label>
										<%-- 	<form:hidden path="image" /> --%>
										<div id="dZUpload" class="dropzone"
											data-upload='<c:url value="<%=UrlConstants.URL_FILE + UrlConstants.URL_UPLOAD%>"/>'
											data-load='<c:url value="<%=UrlConstants.URL_FILE + UrlConstants.URL_LOAD%>"/>'>
											<div class="dz-default dz-message"></div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Thời lượng (phút)</label> <input type="number"
											name="timeCount" class="form-control" />
									</div>
									<div class="form-group">
										<label>Khóa học</label> <select name="" id=""
											class="form-control">
											<option value="">Java script căn bản</option>
											<option value="">Spring MVC</option>
											<option value="">Spring Boot</option>
										</select>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group mt-3">
										<button type="submit" class="btn btn-primary m-b-0">Lưu
											lại</button>
										<a href='<c:url value="<%=UrlConstants.URL_VIDEO%>" />'
											class="btn btn-secondary text-white">Quay lại</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>