package com.myclass.dto;

import javax.validation.constraints.NotBlank;

public class VideoDto {
	private Integer id;
	@NotBlank
	private String title;
	@NotBlank
	private String url;
	private Integer time_count;
	private int course_id;

	private CourseDto course;
}
