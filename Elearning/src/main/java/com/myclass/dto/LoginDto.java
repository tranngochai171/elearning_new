package com.myclass.dto;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class LoginDto extends User{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fullname;
	private String avatar;
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public LoginDto(String email, String password, Collection<? extends GrantedAuthority> authorities) {
		super(email, password, authorities);
		
	}
	
	
}
