package com.myclass.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myclass.dto.RoleDto;
import com.myclass.entity.Role;
import com.myclass.repository.RoleRepository;
import com.myclass.service.RoleService;
import com.myclass.util.UrlConstants;

@RestController
@RequestMapping(value = UrlConstants.URL_API + UrlConstants.URL_ROLE)
@CrossOrigin()
public class APIRoleController {
	@Autowired
	private RoleService roleService;
	@Autowired
	private RoleRepository roleRepository;

	@GetMapping("")
	public Object get() {
		try {
			List<Role> listRole = this.roleRepository.findAll();
			return new ResponseEntity<List<Role>>(listRole, HttpStatus.OK);			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@PostMapping("")
	public Object post(@RequestBody RoleDto roleDto) {
		try {
			this.roleService.saveOrUpdate(roleDto);
			return new ResponseEntity<String>("Thêm mới thành công!", HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<String>("Thêm mới thất bại!", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/{id}")
	public Object put(@PathVariable int id, @RequestBody RoleDto role) {
		try {
			if (roleService.findById(id) == null)
				return new ResponseEntity<String>("ID không hợp lệ!", HttpStatus.BAD_REQUEST);
			roleService.saveOrUpdate(role);
			return new ResponseEntity<String>("Cập nhật thành công!", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Cập nhật thất bại!", HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/{id}")
	public Object delete(@PathVariable int id) {
		try {
			roleService.delete(id);
			return new ResponseEntity<String>("Xóa thành công!", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Xóa thất bại!", HttpStatus.BAD_REQUEST);
		}
	}
}
