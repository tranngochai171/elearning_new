package com.myclass.util;

public class UrlConstants {
	// COMMON URL
	public static final String URL_REDIRECT = "redirect:";
	public static final String URL_ADD = "/add";
	public static final String URL_DELETE = "/delete";
	public static final String URL_EDIT = "/edit";
	public static final String URL_SEARCH = "/search";
	public static final String URL_API = "/api";
	public static final String URL_ADMIN = "/admin";
	// ROLE URL
	public static final String URL_ROLE = "/role";
	// USER URL
	public static final String URL_USER = "/user";
	// LOGIN URL
	public static final String URL_LOGIN = "/login";
	// LOGOUT URL
	public static final String URL_LOGOUT = "/logout";
	// CATEGORY URL
	public static final String URL_CATEGORY = "/category";
	// COURSE URL
	public static final String URL_COURSE = "/course";
	// VDIEO URL
	public static final String URL_VIDEO = "/video";
	// UPLOAD URL
	public static final String URL_FILE = "/file";
	public static final String URL_UPLOAD = "/upload";
	public static final String URL_LOAD = "/load";
	// ERROR URL
	public static final String URL_ERROR = "/error";
	public static final String URL_404 = "/404";
	public static final String URL_403 = "/403";
	public static final String URL_400 = "/400";
}
