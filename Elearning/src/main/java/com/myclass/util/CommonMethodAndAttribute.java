package com.myclass.util;

import javax.servlet.http.HttpSession;

import org.springframework.ui.ModelMap;

public class CommonMethodAndAttribute{
	public static final String MSG_ERROR = "msgError";
	public static final String MSG_SUCCESS = "msgSuccess";

	public static void showMessage(HttpSession session, ModelMap model) {
		if (session.getAttribute("msgSuccess") != null) {
			model.addAttribute("msgSuccess", session.getAttribute("msgSuccess"));
			session.removeAttribute("msgSuccess");
		} else if (session.getAttribute("msgError") != null) {
			model.addAttribute("msgError", session.getAttribute("msgError"));
			session.removeAttribute("msgError");
		}
	}
}
