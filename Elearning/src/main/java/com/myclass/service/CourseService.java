package com.myclass.service;

import com.myclass.dto.CourseDto;
import com.myclass.entity.Course;

public interface CourseService extends GenericService<Course, CourseDto> {

}
