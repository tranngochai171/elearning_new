package com.myclass.service;

import java.util.List;

public interface GenericService<TEntity, TDto> {
	public List<TDto> findAll();

	public boolean saveOrUpdate(TDto dto);

	public boolean delete(Integer id);

	public TDto findById(Integer id);

	public List<TDto> search(String keyword);

//	public abstract TDto mappingEntityToDto(TEntity entity);
//
//	public abstract TEntity mappingDtoToEntity(TDto dto);

	public List<TDto> mappingListEntityToListDto(List<TEntity> listEntity);

}
