package com.myclass.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.myclass.dto.CategoryDto;
import com.myclass.entity.Category;
import com.myclass.service.CategoryService;

@Service
public class CategoryServiceImpl extends GenericServiceImpl<Category, CategoryDto> implements CategoryService {

	public List<CategoryDto> search(String keyword) {
		return mappingListEntityToListDto(this.genericRepository.search(keyword));
	}

}
