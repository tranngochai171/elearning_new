package com.myclass.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.myclass.dto.CourseDto;
import com.myclass.entity.Course;
import com.myclass.service.CourseService;

@Service
public class CourseServiceImpl extends GenericServiceImpl<Course, CourseDto> implements CourseService {

	public List<CourseDto> search(String keyword) {
		return this.mappingListEntityToListDto(this.genericRepository.search(keyword));
	}

}
