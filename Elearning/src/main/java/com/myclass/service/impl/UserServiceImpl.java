package com.myclass.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myclass.dto.UserDto;
import com.myclass.entity.User;
import com.myclass.repository.UserRepository;
import com.myclass.service.UserService;

@Service
public class UserServiceImpl extends GenericServiceImpl<User, UserDto> implements UserService {
	@Autowired
	private UserRepository userRepository;

	public UserDto findByEmail(String email) {
		User user = this.userRepository.findByEmail(email);
		if (user != null)
			return this.modelMapper.map(user, UserDto.class);
		return null;
	}

	public List<UserDto> search(String keyword) {
		return this.mappingListEntityToListDto(this.userRepository.search(keyword));
	}

}
