package com.myclass.service;

import com.myclass.dto.RoleDto;
import com.myclass.entity.Role;

public interface RoleService extends GenericService<Role, RoleDto> {

}
