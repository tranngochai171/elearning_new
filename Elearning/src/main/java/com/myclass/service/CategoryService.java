package com.myclass.service;

import com.myclass.dto.CategoryDto;
import com.myclass.entity.Category;

public interface CategoryService extends GenericService<Category, CategoryDto> {

}
