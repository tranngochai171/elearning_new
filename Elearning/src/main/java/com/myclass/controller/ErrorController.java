package com.myclass.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_ERROR)
public class ErrorController {
	@GetMapping(value = UrlConstants.URL_404)
	public String error_404() {
		return PathConstants.PATH_ERROR_404;
	}

	@GetMapping(value = UrlConstants.URL_400)
	public String error_400() {
		return PathConstants.PATH_ERROR_400;
	}

	@GetMapping(value = UrlConstants.URL_403)
	public String error_403() {
		return PathConstants.PATH_ERROR_403;
	}
}
