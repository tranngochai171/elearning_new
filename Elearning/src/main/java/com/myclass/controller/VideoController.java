package com.myclass.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_ADMIN + UrlConstants.URL_VIDEO)
public class VideoController {
	@GetMapping(value = "")
	public String index() {
		return PathConstants.PATH_VIDEO;
	}

	@GetMapping(value = UrlConstants.URL_ADD)
	public String add() {
		return PathConstants.PATH_VIDEO_ADD;
	}
}
