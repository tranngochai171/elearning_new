package com.myclass.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.myclass.dto.CourseDto;
import com.myclass.service.CategoryService;
import com.myclass.service.CourseService;
import com.myclass.util.CommonMethodsAndAttributes;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_ADMIN + UrlConstants.URL_COURSE)
public class CourseController {
	@Autowired
	private CourseService courseService;
	@Autowired
	private CategoryService categoryService;

	@GetMapping(value = "")
	public String index(HttpSession session, ModelMap model) {
		CommonMethodsAndAttributes.showMessage(session, model);
		model.addAttribute("listCourseDto", this.courseService.findAll());
		return PathConstants.PATH_COURSE;
	}

	@GetMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model) {
		model.addAttribute("courseDto", new CourseDto());
		model.addAttribute("listCategory", this.categoryService.findAll());
		return PathConstants.PATH_COURSE_ADD;
	}

	@PostMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model, HttpSession session, @Validated @ModelAttribute("courseDto") CourseDto courseDto,
			BindingResult errors) {
		if (errors.hasErrors() || !this.courseService.saveOrUpdate(courseDto)) {
			model.addAttribute("message", "(*) Thêm mới thất bại");
			model.addAttribute("courseDto", courseDto);
			model.addAttribute("listCategory", this.categoryService.findAll());
			return PathConstants.PATH_COURSE_ADD;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Thêm mới thành công");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_COURSE;
	}

	@GetMapping(value = UrlConstants.URL_DELETE)
	public String delete(@RequestParam("id") int id, HttpSession session) {
		if (this.courseService.delete(id))
			session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Xóa thành công");
		else
			session.setAttribute(CommonMethodsAndAttributes.MSG_ERROR, "Xóa thất bại");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_COURSE;
	}

	@GetMapping(value = UrlConstants.URL_EDIT)
	public String edit(@RequestParam("id") int id, ModelMap model, HttpSession session) {
		CourseDto courseDto = this.courseService.findById(id);
		if (courseDto != null) {
			model.addAttribute("courseDto", courseDto);
			model.addAttribute("listCategory", this.categoryService.findAll());
			return PathConstants.PATH_COURSE_EDIT;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_ERROR, "Không tìm thấy khóa học");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_EDIT;
	}
}
