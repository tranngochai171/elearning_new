package com.myclass.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_ADMIN + UrlConstants.URL_LOGIN)
public class LoginController {

	@GetMapping(value = "")
	public String index(@RequestParam(required = false, name = "error") String error, ModelMap model) {
		if (error != null && error.equals("true") && !error.isEmpty())
			model.addAttribute("message", "Đăng nhập thất bại");
		return PathConstants.PATH_LOGIN;
	}

	@PostMapping(value = "")
	public String index(ModelMap model) {
		System.out.println(123456);
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_ROLE;
	}
}
