package com.myclass.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.myclass.dto.RoleDto;
import com.myclass.service.RoleService;
import com.myclass.util.CommonMethodsAndAttributes;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_ADMIN + UrlConstants.URL_ROLE)
public class RoleController {
	@Autowired
	private RoleService roleService;

	@GetMapping(value = "")
	public String index(HttpSession session, ModelMap model) {
		CommonMethodsAndAttributes.showMessage(session, model);
		model.addAttribute("listRoleDto", this.roleService.findAll());
		return PathConstants.PATH_ROLE;
	}

	@GetMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model) {
		model.addAttribute("roleDto", new RoleDto());
		return PathConstants.PATH_ROLE_ADD;
	}

	@PostMapping(value = UrlConstants.URL_ADD)
	public String add(HttpSession session, ModelMap model, @Validated @ModelAttribute("roleDto") RoleDto roleDto,
			BindingResult errors) {
		if (errors.hasErrors() || !this.roleService.saveOrUpdate(roleDto)) {
			model.addAttribute("roleDto", roleDto);
			model.addAttribute("message", "(*) Thêm mới thất bại");
			return PathConstants.PATH_ROLE_ADD;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Thêm mới thành công");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_ROLE;
	}

	@GetMapping(value = UrlConstants.URL_DELETE)
	public String delete(HttpSession session, @RequestParam("id") int id) {
		if (!this.roleService.delete(id)) {
			session.setAttribute(CommonMethodsAndAttributes.MSG_ERROR, "Xóa role thất bại");
		} else {
			session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Xóa role thành công");
		}
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_ROLE;
	}

	@GetMapping(value = UrlConstants.URL_EDIT)
	public String edit(HttpSession session, @RequestParam("id") int id, ModelMap model) {
		RoleDto roleDto = this.roleService.findById(id);
		if (roleDto != null) {
			model.addAttribute("roleDto", roleDto);
			return PathConstants.PATH_ROLE_EDIT;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_ERROR, "Không tìm thấy role");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_ROLE;
	}

	@PostMapping(value = UrlConstants.URL_EDIT)
	public String edit(HttpSession session, ModelMap model, @Validated @ModelAttribute("roleDto") RoleDto roleDto,
			BindingResult errors) {
		if (errors.hasErrors() || !this.roleService.saveOrUpdate(roleDto)) {
			model.addAttribute("message", "(*) Cập nhật thất bại");
			model.addAttribute("role", roleDto);
			return PathConstants.PATH_ROLE_EDIT;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Sửa role thành công");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_ROLE;
	}

	@GetMapping(value = UrlConstants.URL_SEARCH)
	public String search(ModelMap model, @RequestParam("keyword") String keyword) {
		model.addAttribute("listRoleDto", this.roleService.search(keyword));
		return PathConstants.PATH_ROLE;
	}
}
