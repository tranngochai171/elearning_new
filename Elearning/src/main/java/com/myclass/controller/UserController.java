package com.myclass.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.myclass.dto.UserDto;
import com.myclass.service.RoleService;
import com.myclass.service.UserService;
import com.myclass.util.CommonMethodsAndAttributes;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;
import com.myclass.validation.UserDtoValidator;

@Controller
@RequestMapping(value = UrlConstants.URL_ADMIN + UrlConstants.URL_USER)
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private UserDtoValidator userDtoValidator;

	@GetMapping(value = "")
	public String index(HttpSession session, ModelMap model) {
		CommonMethodsAndAttributes.showMessage(session, model);
		model.addAttribute("listUserDto", this.userService.findAll());
		return PathConstants.PATH_USER;
	}

	@GetMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model) {
		model.addAttribute("listRoleDto", this.roleService.findAll());
		model.addAttribute("userDto", new UserDto());
		return PathConstants.PATH_USER_ADD;
	}

	@PostMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model, HttpSession session, @Validated @ModelAttribute("userDto") UserDto userDto,
			BindingResult errors) {
		this.userDtoValidator.validate(userDto, errors);
		if (errors.hasErrors() || !this.userService.saveOrUpdate(userDto)) {
			model.addAttribute("userDto", userDto);
			model.addAttribute("message", "(*) Thêm mới thất bại");
			model.addAttribute("listRoleDto", roleService.findAll());
			return PathConstants.PATH_USER_ADD;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Thêm mới user thành công");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_USER;
	}

	@GetMapping(value = UrlConstants.URL_DELETE)
	public String delete(@RequestParam("id") int id, HttpSession session) {
		if (!this.userService.delete(id)) {
			session.setAttribute(CommonMethodsAndAttributes.MSG_ERROR, "Xóa User thất bại");
		} else {
			session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Xóa User thành công");
		}
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_USER;
	}

	@GetMapping(value = UrlConstants.URL_EDIT)
	public String edit(HttpSession session, @RequestParam("id") int id, ModelMap model) {
		UserDto userDto = this.userService.findById(id);
		if (userDto != null) {
			model.addAttribute("listRoleDto", this.roleService.findAll());
			model.addAttribute("userDto", userDto);
			return PathConstants.PATH_USER_EDIT;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_ERROR, "Không tìm thấy user");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_USER;
	}

	@PostMapping(value = UrlConstants.URL_EDIT)
	public String edit(ModelMap model, HttpSession session, @Valid @ModelAttribute("userDto") UserDto userDto,
			BindingResult errors) {
		if (errors.hasErrors() || !this.userService.saveOrUpdate(userDto)) {
			model.addAttribute("listRoleDto", this.roleService.findAll());
			model.addAttribute("user", userDto);
			model.addAttribute("message", "(*) Sửa thông tin thất bại");
			return PathConstants.PATH_USER_EDIT;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Sửa thông tin thành công");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_USER;
	}

	@GetMapping(value = UrlConstants.URL_SEARCH)
	public String search(@RequestParam("keyword") String keyword, ModelMap model) {
		model.addAttribute("listUserDto", this.userService.search(keyword));
		return PathConstants.PATH_USER;
	}
}
