package com.myclass.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.myclass.util.UrlConstants;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "com.myclass")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDetailsService userDetailsService;

	// khai báo đối tượng giải mã pass và check pass
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	// Khai báo lớp service dùng để check email lấy thông tin người dùng
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.userDetailsService).passwordEncoder(this.passwordEncoder());
	}

	// phân quyền người dùng
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// Cấu hình phân quyền
		http.csrf().disable().antMatcher("/admin/**").authorizeRequests()
				.antMatchers(UrlConstants.URL_ADMIN+"/**").permitAll();// cấu
				// hình
				// các
				// url cho phép
				// truy cập
				// không cần
				// đăng nhập
//				.antMatchers(UrlConstants.URL_ADMIN + UrlConstants.URL_ROLE).hasAnyRole("ADMIN").anyRequest()
//				.authenticated();// bắt buộc phải
		// đăng nhập rồi
		// mới được vào

		// Điều hướng về trang ko có quyền
		http.exceptionHandling().accessDeniedPage(UrlConstants.URL_ERROR + UrlConstants.URL_403);

		// cấu hình đăng xuất
		http.logout().logoutUrl(UrlConstants.URL_ADMIN + UrlConstants.URL_LOGOUT)
				.logoutSuccessUrl(UrlConstants.URL_ADMIN + UrlConstants.URL_LOGIN).deleteCookies("JSESSIONID");

		// Cấu hình đăng nhập
		http.formLogin().loginPage(UrlConstants.URL_ADMIN + UrlConstants.URL_LOGIN) // link của hàm get dùng để show
																					// form đăng nhập
				.loginProcessingUrl(UrlConstants.URL_ADMIN + UrlConstants.URL_LOGIN) // link khai báo trong action của
																						// form đăng nhập
				.usernameParameter("email") // name của ô input dùng để nhập email
				.passwordParameter("password") // name của ô input dùng để nhập password
				.defaultSuccessUrl(UrlConstants.URL_ADMIN + UrlConstants.URL_CATEGORY)
				.failureUrl(UrlConstants.URL_ADMIN + UrlConstants.URL_LOGIN + "?error=true");

	}
}
