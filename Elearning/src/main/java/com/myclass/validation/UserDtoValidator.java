package com.myclass.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.myclass.dto.UserDto;
import com.myclass.service.UserService;

@Component
public class UserDtoValidator implements Validator {
	@Autowired
	private UserService userService;

	public boolean supports(Class<?> clazz) {
		return UserDto.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		UserDto userDto = (UserDto) target;
		if (this.userService.findByEmail(userDto.getEmail()) != null)
			errors.rejectValue("email", "userDto", "(*) Email đã tồn tại");
		if (userDto.getConfirm().equals("")) {
			errors.rejectValue("confirm", "userDto", "(*) Vui lòng nhập xác nhận mật khẩu");
		}
		if (!userDto.getPassword().equals(userDto.getConfirm()))
			errors.rejectValue("confirm", "userDto", "(*) Mật khẩu xác nhận chưa chính xác");
	}

}
