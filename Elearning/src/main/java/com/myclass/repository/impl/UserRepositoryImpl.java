package com.myclass.repository.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Repository;

import com.myclass.entity.User;
import com.myclass.repository.UserRepository;

@Repository
@Transactional(rollbackOn = Exception.class)
public class UserRepositoryImpl extends GenericRepositoryImpl<User> implements UserRepository {
	public List<User> search(String keyword) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String hql = "FROM User WHERE fullname LIKE :fullname OR email LIKE :email";
			Query<User> query = session.createQuery(hql, User.class);
			query.setParameter("fullname", "%" + keyword + "%");
			query.setParameter("email", "%" + keyword + "%");
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public User findByEmail(String email) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String hql = "FROM User WHERE email = :email";
			Query<User> query = session.createQuery(hql, User.class);
			query.setParameter("email", email);
			return query.getResultList().get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<User> findPaging(int pageIndex, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<User> search(String keyword, int pageIndex, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean saveOrUpdate(User entity) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String hashed = BCrypt.hashpw(entity.getPassword(), BCrypt.gensalt());
			entity.setPassword(hashed);
			session.saveOrUpdate(entity);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
