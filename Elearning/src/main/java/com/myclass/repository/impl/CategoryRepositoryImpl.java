package com.myclass.repository.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.myclass.entity.Category;
import com.myclass.repository.CategoryRepository;

@Repository
@Transactional(rollbackOn = Exception.class)
public class CategoryRepositoryImpl extends GenericRepositoryImpl<Category> implements CategoryRepository {

	public List<Category> search(String keyword) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String hql = "FROM Category WHERE title LIKE :title";
			Query<Category> query = session.createQuery(hql, Category.class);
			query.setParameter("title", "%" + keyword + "%");
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Category> findPaging(int pageIndex, int pageSize) {
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "FROM Category";
		try {
			Query<Category> query = session.createQuery(hql, Category.class);
			query.setFirstResult((pageIndex - 1) * pageSize);
			query.setMaxResults(pageSize);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Category> search(String keyword, int pageIndex, int pageSize) {
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "FROM Category WHERE title LIKE :title";
		try {
			Query<Category> query = session.createQuery(hql, Category.class);
			query.setParameter("title", "%" + keyword + "%");
			query.setFirstResult((pageIndex - 1) * pageSize);
			query.setMaxResults(pageSize);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
